package com.mramirid.moviecatalogue.tab;


import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mramirid.moviecatalogue.R;
import com.mramirid.moviecatalogue.activity.ItemDescriptionActivity;
import com.mramirid.moviecatalogue.adapter.ItemAdapter;
import com.mramirid.moviecatalogue.pojo.Item;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MoviesFragment extends Fragment {

    private ArrayList<Item> movies;
    private TypedArray moviesDataPhoto;
    private String[] moviesDataName, moviesDataGenres, moviesDataDuration, moviesDataDescription;
    private int[] moviesDataYear;

    private ItemAdapter moviesAdapter;

    public MoviesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_movies, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.rv_movies);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        moviesAdapter.setListItem(movies);
        recyclerView.setAdapter(moviesAdapter);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prepare();
        addItem();

        moviesAdapter = new ItemAdapter(getActivity());

        moviesAdapter.setOnItemClickCallback(new ItemAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(Item item) {
                Intent moveToDescription = new Intent(getActivity(), ItemDescriptionActivity.class);
                moveToDescription.putExtra(ItemDescriptionActivity.KEY_EXTRA, item);
                startActivity(moveToDescription);
            }
        });
    }

    private void prepare() {
        moviesDataPhoto = getResources().obtainTypedArray(R.array.movies_data_photo);
        moviesDataName = getResources().getStringArray(R.array.movies_data_name);
        moviesDataGenres = getResources().getStringArray(R.array.movies_data_genres);
        moviesDataYear = getResources().getIntArray(R.array.movies_data_year);
        moviesDataDuration = getResources().getStringArray(R.array.movies_data_duration);
        moviesDataDescription = getResources().getStringArray(R.array.movies_data_description);
    }

    private void addItem() {
        movies = new ArrayList<>();

        for (int i = 0; i < moviesDataName.length; ++i) {
            Item movie = new Item();
            movie.setPhoto(moviesDataPhoto.getResourceId(i, -1));
            movie.setName(moviesDataName[i]);
            movie.setGenres(moviesDataGenres[i]);
            movie.setYear(moviesDataYear[i]);
            movie.setDuration(moviesDataDuration[i]);
            movie.setDescription(moviesDataDescription[i]);
            movies.add(movie);
        }
    }
}
