package com.mramirid.moviecatalogue.tab;


import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mramirid.moviecatalogue.R;
import com.mramirid.moviecatalogue.activity.ItemDescriptionActivity;
import com.mramirid.moviecatalogue.adapter.ItemAdapter;
import com.mramirid.moviecatalogue.pojo.Item;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class TvShowsFragment extends Fragment {

    private ArrayList<Item> tvShows;
    private TypedArray tvShowsDataPhoto;
    private String[] tvShowsDataName, tvShowsDataGenres, tvShowsDataDuration, tvShowsDataDescription;
    private int[] tvShowsDataYear;

    private ItemAdapter tvShowsAdapter;

    public TvShowsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tv_shows, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.rv_tv_shows);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        tvShowsAdapter.setListItem(tvShows);
        recyclerView.setAdapter(tvShowsAdapter);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prepare();
        addItem();

        tvShowsAdapter = new ItemAdapter(getActivity());

        tvShowsAdapter.setOnItemClickCallback(new ItemAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(Item item) {
                Intent moveToDescription = new Intent(getActivity(), ItemDescriptionActivity.class);
                moveToDescription.putExtra(ItemDescriptionActivity.KEY_EXTRA, item);
                startActivity(moveToDescription);
            }
        });
    }

    private void prepare() {
        tvShowsDataPhoto = getResources().obtainTypedArray(R.array.tvshows_data_photo);
        tvShowsDataName = getResources().getStringArray(R.array.tvshows_data_name);
        tvShowsDataGenres = getResources().getStringArray(R.array.tvshows_data_genres);
        tvShowsDataYear = getResources().getIntArray(R.array.tvshows_data_year);
        tvShowsDataDuration = getResources().getStringArray(R.array.tvshows_data_duration);
        tvShowsDataDescription = getResources().getStringArray(R.array.tvshows_data_description);
    }

    private void addItem() {
        tvShows = new ArrayList<>();

        for (int i = 0; i < tvShowsDataName.length; ++i) {
            Item tvShow = new Item();
            tvShow.setPhoto(tvShowsDataPhoto.getResourceId(i, -1));
            tvShow.setName(tvShowsDataName[i]);
            tvShow.setGenres(tvShowsDataGenres[i]);
            tvShow.setYear(tvShowsDataYear[i]);
            tvShow.setDuration(tvShowsDataDuration[i]);
            tvShow.setDescription(tvShowsDataDescription[i]);
            tvShows.add(tvShow);
        }
    }
}
