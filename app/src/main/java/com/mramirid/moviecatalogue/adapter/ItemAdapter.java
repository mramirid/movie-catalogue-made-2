package com.mramirid.moviecatalogue.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mramirid.moviecatalogue.R;
import com.mramirid.moviecatalogue.pojo.Item;

import java.util.ArrayList;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.CategoryViewHolder> {

    private Context context;
    private ArrayList<Item> listItem;

    private OnItemClickCallback onItemClickCallback;

    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    public ItemAdapter(Context context) {
        this.context = context;
    }

    private ArrayList<Item> getListItem() {
        return listItem;
    }

    public void setListItem(ArrayList<Item> listItem) {
        this.listItem = listItem;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new CategoryViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoryViewHolder holder, int position) {
        holder.txtName.setText(getListItem().get(position).getName());
        holder.txtGenres.setText(getListItem().get(position).getGenres());
        holder.txtYear.setText(String.valueOf(getListItem().get(position).getYear()));
        holder.txtDuration.setText(getListItem().get(position).getDuration());
        Glide.with(context)
                .load(getListItem().get(position).getPhoto())
                .into(holder.imgPhoto);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickCallback.onItemClicked(listItem.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return getListItem().size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {

        ImageView imgPhoto;
        TextView txtName, txtGenres, txtYear, txtDuration;

        CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.img_photo);
            txtName = itemView.findViewById(R.id.txt_name);
            txtGenres = itemView.findViewById(R.id.txt_genres);
            txtYear = itemView.findViewById(R.id.txt_year);
            txtDuration = itemView.findViewById(R.id.txt_duration);
        }
    }

    public interface OnItemClickCallback {
        void onItemClicked(Item item);
    }
}
