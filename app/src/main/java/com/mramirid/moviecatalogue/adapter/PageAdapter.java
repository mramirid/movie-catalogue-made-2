package com.mramirid.moviecatalogue.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mramirid.moviecatalogue.tab.MoviesFragment;
import com.mramirid.moviecatalogue.tab.TvShowsFragment;

public class PageAdapter extends FragmentStatePagerAdapter {

    private int countTab;

    public PageAdapter(FragmentManager fm, int countTab) {
        super(fm);
        this.countTab = countTab;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Indeks tab Movies
                return new MoviesFragment();
            case 1: // Indeks tab TV Shows
                return new TvShowsFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return countTab;
    }
}
