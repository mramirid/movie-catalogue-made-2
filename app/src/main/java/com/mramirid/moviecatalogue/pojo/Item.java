package com.mramirid.moviecatalogue.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class Item implements Parcelable {

    private int photo;
    private String name, genres, duration, description;
    private int year;

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(photo);
        parcel.writeString(name);
        parcel.writeString(genres);
        parcel.writeString(duration);
        parcel.writeString(description);
        parcel.writeInt(year);
    }

    public Item() {
    }

    private Item(Parcel in) {
        photo = in.readInt();
        name = in.readString();
        genres = in.readString();
        duration = in.readString();
        description = in.readString();
        year = in.readInt();
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };
}
