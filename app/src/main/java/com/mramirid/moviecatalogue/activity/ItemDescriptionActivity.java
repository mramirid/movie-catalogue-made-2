package com.mramirid.moviecatalogue.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.mramirid.moviecatalogue.R;
import com.mramirid.moviecatalogue.pojo.Item;

public class  ItemDescriptionActivity extends AppCompatActivity {

    public static final String KEY_EXTRA = "key_extra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_description);

        ImageView imgCoverDetail = findViewById(R.id.img_cover_detail);
        ImageView imgPhotoDetail = findViewById(R.id.img_photo_detail);
        TextView txtNameDetail = findViewById(R.id.txt_name_detail);
        TextView txtGenresDetail = findViewById(R.id.txt_genres_detail);
        TextView txtYearDetail = findViewById(R.id.txt_year_detail);
        TextView txtDurationDetail = findViewById(R.id.txt_duration_detail);
        TextView txtDescriptionDetail = findViewById(R.id.txt_description_detail);

        Item item = getIntent().getParcelableExtra(KEY_EXTRA);

        if (item != null) {
            Glide.with(this).load(item.getPhoto()).centerCrop().into(imgCoverDetail);
            Glide.with(this).load(item.getPhoto()).into(imgPhotoDetail);
            txtNameDetail.setText(item.getName());
            txtGenresDetail.setText(item.getGenres());
            txtYearDetail.setText(String.valueOf(item.getYear()));
            txtDurationDetail.setText(item.getDuration());
            txtDescriptionDetail.setText(item.getDescription());
        }

        ImageButton arrow_back = findViewById(R.id.arrow_back);
        arrow_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
